package com.lightwing.server.ftp.session;

import com.lightwing.server.ftp.commands.Command;
import com.lightwing.server.ftp.dataChannel.DataChannel;
import com.lightwing.server.ftp.models.FileWrapper;
import com.lightwing.server.ftp.models.ReceivedCommand;
import com.lightwing.server.ftp.utils.Response;
import com.sun.org.apache.xpath.internal.operations.Bool;

import java.io.*;
import java.net.Socket;
import java.util.List;

public class FtpSession implements Runnable
{
  public static final String EXIT_SESSION_COMMAND = "EXIT_FTP";
  public static final String EOF = "\r\n";
  public static final String CHARSET_NAME = "UTF-8";

  private List<Command> commands;
  private BufferedReader bufferedReader;
  private OutputStreamWriter outputStreamWriter;
  private FileWrapper workingDirectory;
  private DataChannel dataChannel;
  private Boolean loggedIn;

  public FtpSession(Socket commandSocket, List<Command> commands, String initialFtpDir) throws IOException
  {
    this.commands = commands;

    loggedIn = false;
    bufferedReader = new BufferedReader(new InputStreamReader(commandSocket.getInputStream()));
    outputStreamWriter = new OutputStreamWriter(commandSocket.getOutputStream(), CHARSET_NAME);
    workingDirectory = new FileWrapper(new File(initialFtpDir));
    dataChannel = new DataChannel();
  }

  public void run()
  {
    try
    {
      runSession();

      outputStreamWriter.close();
      bufferedReader.close();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
  }

  private void runSession() throws IOException
  {
    boolean continueFlag = true;

    do
    {
      ReceivedCommand receivedCommand = new ReceivedCommand(bufferedReader.readLine());

      if (shouldContinue(receivedCommand))
      {
        respondToClient(receivedCommand);
      }
      else
      {
        continueFlag = false;
      }
    }
    while (continueFlag);
  }

  private boolean shouldContinue(ReceivedCommand receivedCommand)
  {
    return !EXIT_SESSION_COMMAND.equals(receivedCommand.getCommandType());
  }

  private void respondToClient(ReceivedCommand receivedCommand) throws IOException
  {
    String returnMessage = processCommand(receivedCommand.getCommandType(), receivedCommand.getCommandParam());

    outputStreamWriter.write(returnMessage + EOF);
    outputStreamWriter.flush();
  }

  private String processCommand(String commandType, String commandParam) throws IOException
  {
    try
    {
      for (Command command : commands)
      {
        if (command.isMe(commandType))
        {
          if (!command.canAccess(loggedIn))
          {
            return Response.USER_NOT_LOGGED_IN.toString();
          }

          return command.process(commandParam, this);
        }
      }

      return Response.COMMAND_NOT_FOUND.setData(commandType).toString();
    }
    catch (Exception e)
    {
      return Response.UNKNOWN_ERROR.toString();
    }
  }

  public FileWrapper getFileWrapper()
  {
    return this.workingDirectory;
  }

  public DataChannel getDataChannel()
  {
    return dataChannel;
  }

  public void setLoggedIn()
  {
    loggedIn = true;
  }
}