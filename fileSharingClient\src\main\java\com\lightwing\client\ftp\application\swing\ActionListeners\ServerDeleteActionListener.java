package com.lightwing.client.ftp.application.swing.ActionListeners;

import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.DirectoryListElement;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.model.Response;
import com.lightwing.client.ftp.application.swing.view.ClientView;

import java.awt.event.ActionEvent;
import java.io.IOException;


public class ServerDeleteActionListener extends AbstractActionListener
{
  public ServerDeleteActionListener(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, ClientView clientView)
  {
    super(clientController, ftpSession, ftpClient, clientView);
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
    try
    {
      if (clientView.getSeverFileList().getSelectedValue() instanceof DirectoryListElement)
      {
        deleteTarget();
      }
      else
      {
        clientView.showMessageBox("Invalid item selected");
      }
    }
    catch (IOException e1)
    {
      clientView.showMessageBox("Error sending request to server");
    }
  }

  private void deleteTarget() throws IOException
  {
    DirectoryListElement target = (DirectoryListElement)clientView.getSeverFileList().getSelectedValue();

    if (target.isDirectory())
    {
      Response response = new Response(ftpClient.removeDirectory(ftpSession.getServerWorkingDirectory() + "/" + target.getName()));
      updateView(response);
    }
    else
    {
      Response response = new Response(ftpClient.deleteFile(ftpSession.getServerWorkingDirectory() + "/" + target.getName()));
      updateView(response);
    }
  }
}
