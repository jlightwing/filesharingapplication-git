package com.lightwing.client.ftp.application.swing.ActionListeners;

import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.DirectoryListElement;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.application.swing.view.ClientView;
import com.lightwing.client.ftp.application.swing.view.progressBar.ProgressChangeListener;
import com.lightwing.client.ftp.dataTransfer.StoreSwingWorker;

import java.awt.event.ActionEvent;
import java.io.IOException;

public class StoreActionListener extends AbstractActionListener
{
  public StoreActionListener(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, ClientView clientView)
  {
    super(clientController, ftpSession, ftpClient, clientView);
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
    try
    {
      if (clientView.getClientFileList().getSelectedValue() instanceof DirectoryListElement)
      {
        DirectoryListElement targetFile = (DirectoryListElement) clientView.getClientFileList().getSelectedValue();

        if (!targetFile.isDirectory())
        {
          if (setupDataChannel())
          {
            ProgressChangeListener progressChangeListener = new ProgressChangeListener(clientView.getFrame());
            StoreSwingWorker storeSwingWorker = new StoreSwingWorker(clientController, ftpSession, ftpClient, progressChangeListener.getProgressFrame(), targetFile);
            storeSwingWorker.addPropertyChangeListener(progressChangeListener);
            storeSwingWorker.execute();
          }
          else
          {
            clientView.showMessageBox("Data channel could not be opened");
          }
        }
        else
        {
          clientView.showMessageBox("A file is not selected");
        }
      }
      else
      {
        clientView.showMessageBox("Invalid item selected");
      }
    }
    catch (IOException e1)
    {
      clientView.showMessageBox("Error sending request to server");
    }
  }
}
