package com.lightwing.server.ftp;

import com.lightwing.server.ftp.commands.*;
import com.lightwing.server.ftp.session.FtpSession;

import java.io.File;
import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

public class FtpServerImpl implements FtpServer
{
  private ServerSocket serverSocket;
  private int port;
  private String username;
  private String password;
  private String[] args;
  private String initialFtpDir;

  private static final String SERVER_BASE_DIRECTORY = "/ftp/server";

  public FtpServerImpl(String[] args)
  {
    this.args = args;
  }

  public void run() throws IOException
  {
    getConfigFromArgs();
    if (setupFiles())
    {
      this.serverSocket = new ServerSocket(port);

      while (true)
      {
        List<Command> commands = new ArrayList<Command>();
        commands.add(new PrintWorkingDirectoryCommand());
        commands.add(new ChangeWorkingDirectoryCommand());
        commands.add(new MakeDirectoryCommand());
        commands.add(new RemoveDirectoryCommand());
        commands.add(new ChangeToParentDirectoryCommand());
        commands.add(new DeleteFileCommand());
        commands.add(new StoreCommand());
        commands.add(new PasvCommand());
        commands.add(new RetrieveCommand());
        commands.add(new StatusCommand());
        commands.add(new ListCommand());
        commands.add(new UserCommand(username, password));

        FtpSession ftpSession = new FtpSession(serverSocket.accept(), commands, initialFtpDir);
        new Thread(ftpSession).start();
      }
    }
    else
    {
      System.out.println("FTP directory can not be created");
    }
  }

  private void getConfigFromArgs()
  {
    port = 9595;
    username = "user";
    password = "password";

    if (args.length != 0)
    {
      for (int x = 0; x < args.length; x ++)
      {
        if ("-p".equals(args[x]) && (x + 1 < args.length) && args[x+1].matches("^\\d+$"))
        {
          port = Integer.parseInt(args[x+1]);
        }
        else if ("-u".equals(args[x]) && (x + 1 < args.length))
        {
          username = args[x+1];
        }
        else if ("-P".equals(args[x]) && (x + 1 < args.length))
        {
          password = args[x+1];
        }
      }
    }
  }

  private boolean setupFiles()
  {
    String homeDir = System.getProperty("user.home");
    File baseDirectory = new File(homeDir + SERVER_BASE_DIRECTORY);

    if (!baseDirectory.exists())
    {
      baseDirectory.mkdirs();
      initialFtpDir = baseDirectory.getPath();
      return true;
    }
    else if (baseDirectory.exists() && baseDirectory.isDirectory())
    {
      initialFtpDir = baseDirectory.getPath();
      return true;
    }
    else
    {
      return false;
    }
  }
}
