package com.lightwing.client.ftp.dataTransfer;

import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.DirectoryListElement;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.model.Response;
import com.lightwing.client.ftp.application.swing.view.ClientView;
import com.lightwing.client.ftp.application.swing.view.progressBar.StatusContainer;

import javax.swing.*;
import java.io.IOException;


public class RetrieveSwingWorker extends SwingWorker<Void, Void>
{
  private FtpSession ftpSession;
  private FtpClient ftpClient;
  private JFrame progressFrame;
  private DirectoryListElement targetFile;
  private ClientController clientController;

  public RetrieveSwingWorker(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, JFrame progressFrame, DirectoryListElement targetFile)
  {
    this.ftpSession = ftpSession;
    this.ftpClient = ftpClient;
    this.progressFrame = progressFrame;
    this.targetFile = targetFile;
    this.clientController = clientController;
  }

  @Override
  protected Void doInBackground() throws Exception
  {
    try
    {
      StatusContainer statusContainer = new StatusContainer(targetFile.getSize());

      Response retrieveResponse = new Response(ftpClient.retrieveFile(ftpSession.getClientWorkingDirectory(), targetFile.getName(), ftpSession.getServerDataPort(), statusContainer));

      if (Response.SUCCESS.equals(retrieveResponse.getCode()))
      {
        retrieveFile(statusContainer);
      }
      else
      {
        JOptionPane.showMessageDialog(progressFrame, retrieveResponse.getDescription());
      }

      returnToMainView();
    }
    catch (IOException e1)
    {
      JOptionPane.showMessageDialog(progressFrame, "Error sending request to server");
      returnToMainView();
    }

    return null;
  }

  private void retrieveFile(StatusContainer statusContainer) throws IOException
  {
    Response statusResponse = new Response(ftpClient.status());

    while (Response.STATUS_IN_PROGRESS.equals(statusResponse.getCode()))
    {
      setProgress(statusContainer.getProgress());
      statusResponse = new Response(ftpClient.status());
    }

    if (Response.STATUS_SUCCESS.equals(statusResponse.getCode()))
    {
      JOptionPane.showMessageDialog(progressFrame, statusResponse.getDescription());
    }
    else if (Response.STATUS_FAILURE.equals(statusResponse.getCode()))
    {
      JOptionPane.showMessageDialog(progressFrame, statusResponse.getDescription());
    }
  }

  private void returnToMainView()
  {
    clientController.updateClientView();

    progressFrame.setVisible(false);
    progressFrame.dispose();
  }
}
