package com.lightwing.client.ftp.application.swing.view;

import java.awt.*;
import javax.swing.*;

public class ClientView extends JPanel
{
  private JTextField clientWorkingDirectory;
  private JTextField serverWorkingDirectory;
  private JScrollPane clientFileListScrollPane;
  private JList clientFileList;
  private JPanel panel;
  private JButton uploadButton;
  private JButton downloadButton;
  private JScrollPane serverFileListScrollPane;
  private JList severFileList;
  private JButton clientParentButton;
  private JButton serverParentButton;
  private JButton clientOpenButton;
  private JButton serverOpenButton;
  private JButton clientNewFolderButton;
  private JButton serverNewFolderButton;
  private JButton clientDeleteButton;
  private JButton serverDeleteButton;
  private JFrame frame;

  public ClientView(JFrame frame)
  {
    this.frame = frame;
    initComponents();
  }

  private void initComponents()
  {
    clientWorkingDirectory = new JTextField();
    serverWorkingDirectory = new JTextField();
    clientFileListScrollPane = new JScrollPane();
    clientFileList = new JList();
    panel = new JPanel();
    uploadButton = new JButton();
    downloadButton = new JButton();
    serverFileListScrollPane = new JScrollPane();
    severFileList = new JList();
    clientParentButton = new JButton();
    serverParentButton = new JButton();
    clientOpenButton = new JButton();
    serverOpenButton = new JButton();
    clientNewFolderButton = new JButton();
    serverNewFolderButton = new JButton();
    clientDeleteButton = new JButton();
    serverDeleteButton = new JButton();

    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[]{146, 0, 158, 0};
    ((GridBagLayout) getLayout()).rowHeights = new int[]{0, 269, 0, 0, 0, 0, 0, 0};
    ((GridBagLayout) getLayout()).columnWeights = new double[]{0.0, 0.0, 0.0, 1.0E-4};
    ((GridBagLayout) getLayout()).rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};
    add(clientWorkingDirectory, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
    add(serverWorkingDirectory, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

    {
      clientFileListScrollPane.setViewportView(clientFileList);
    }
    add(clientFileListScrollPane, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

    {
      panel.setLayout(new GridBagLayout());
      ((GridBagLayout) panel.getLayout()).columnWidths = new int[]{0, 0};
      ((GridBagLayout) panel.getLayout()).rowHeights = new int[]{0, 0, 0, 0};
      ((GridBagLayout) panel.getLayout()).columnWeights = new double[]{0.0, 1.0E-4};
      ((GridBagLayout) panel.getLayout()).rowWeights = new double[]{0.0, 0.0, 0.0, 1.0E-4};

      uploadButton.setText(">>");
      panel.add(uploadButton, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 5, 0), 0, 0));

      downloadButton.setText("<<");
      panel.add(downloadButton, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
              GridBagConstraints.CENTER, GridBagConstraints.BOTH,
              new Insets(0, 0, 0, 0), 0, 0));
    }
    add(panel, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

    {
      serverFileListScrollPane.setViewportView(severFileList);
    }
    add(serverFileListScrollPane, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

    clientParentButton.setText("../");
    add(clientParentButton, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

    serverParentButton.setText("../");
    add(serverParentButton, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

    clientOpenButton.setText("Open");
    add(clientOpenButton, new GridBagConstraints(0, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

    serverOpenButton.setText("Open");
    add(serverOpenButton, new GridBagConstraints(2, 4, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

    clientNewFolderButton.setText("New Folder");
    add(clientNewFolderButton, new GridBagConstraints(0, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

    serverNewFolderButton.setText("New Folder");
    add(serverNewFolderButton, new GridBagConstraints(2, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

    clientDeleteButton.setText("Delete");
    add(clientDeleteButton, new GridBagConstraints(0, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 5), 0, 0));

    serverDeleteButton.setText("Delete");
    add(serverDeleteButton, new GridBagConstraints(2, 6, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 0, 0), 0, 0));
  }

  public void showMessageBox(String message)
  {
    JOptionPane.showMessageDialog(this, message);
  }

  public String showInputBox(String message)
  {
    return JOptionPane.showInputDialog(this, message, null);
  }

  public JTextField getClientWorkingDirectory()
  {
    return clientWorkingDirectory;
  }

  public JTextField getServerWorkingDirectory()
  {
    return serverWorkingDirectory;
  }

  public JScrollPane getClientFileListScrollPane()
  {
    return clientFileListScrollPane;
  }

  public JList getClientFileList()
  {
    return clientFileList;
  }

  public JPanel getPanel()
  {
    return panel;
  }

  public JButton getUploadButton()
  {
    return uploadButton;
  }

  public JButton getDownloadButton()
  {
    return downloadButton;
  }

  public JScrollPane getServerFileListScrollPane()
  {
    return serverFileListScrollPane;
  }

  public JList getSeverFileList()
  {
    return severFileList;
  }

  public JButton getClientParentButton()
  {
    return clientParentButton;
  }

  public JButton getServerParentButton()
  {
    return serverParentButton;
  }

  public JButton getClientOpenButton()
  {
    return clientOpenButton;
  }

  public JButton getServerOpenButton()
  {
    return serverOpenButton;
  }

  public JButton getClientNewFolderButton()
  {
    return clientNewFolderButton;
  }

  public JButton getServerNewFolderButton()
  {
    return serverNewFolderButton;
  }

  public JButton getClientDeleteButton()
  {
    return clientDeleteButton;
  }

  public JButton getServerDeleteButton()
  {
    return serverDeleteButton;
  }

  public JFrame getFrame()
  {
    return frame;
  }

}
