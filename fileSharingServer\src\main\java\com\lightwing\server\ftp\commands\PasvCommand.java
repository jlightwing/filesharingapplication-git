package com.lightwing.server.ftp.commands;

import com.lightwing.server.ftp.session.FtpSession;
import com.lightwing.server.ftp.utils.Response;

import java.io.IOException;
import java.net.ServerSocket;

public class PasvCommand implements Command
{
  public static final String COMMAND = "PASV";

  public boolean isMe(String commandType)
  {
    return COMMAND.equals(commandType);
  }

  public boolean canAccess(boolean loggedIn)
  {
    return loggedIn;
  }

  public String process(String commandParam, FtpSession ftpSession)
  {
    try
    {
      return Response.PASV_SUCCESS.setData(String.valueOf(ftpSession.getDataChannel().initialiseDataSocket())).toString();
    }
    catch (IOException e)
    {
      return Response.PASV_FAIL.toString();
    }
  }
}
