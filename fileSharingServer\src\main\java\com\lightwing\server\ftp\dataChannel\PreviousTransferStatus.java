package com.lightwing.server.ftp.dataChannel;

public enum PreviousTransferStatus
{
  SUCCESS("SUCCESS"),
  FAILURE("FAILURE"),
  NO_TRANSFER("NO_TRANSFER");

  private String code;

  PreviousTransferStatus(String code)
  {
    this.code = code;
  }

  @Override
  public String toString()
  {
    return code;
  }
}
