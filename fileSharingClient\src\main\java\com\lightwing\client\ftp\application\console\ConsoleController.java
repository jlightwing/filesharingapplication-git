package com.lightwing.client.ftp.application.console;

import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpClientImpl;
import com.lightwing.client.ftp.model.Response;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.application.swing.view.progressBar.StatusContainer;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ConsoleController
{
  public static final String HOST = "localhost";
  public static final int PORT = 9595;

  private BufferedReader bufferedReader;
  private FtpClient ftpClient;
  private FtpSession ftpSession;

  public ConsoleController() throws IOException
  {
    bufferedReader = new BufferedReader(new InputStreamReader(System.in));
    ftpClient = new FtpClientImpl(new Socket(HOST, PORT), HOST);
    ftpSession = new FtpSession();
  }

  public void run() throws IOException
  {
    boolean continueFlag = true;

    do
    {
      String input = bufferedReader.readLine();
      UserInput userInput = new UserInput(input);

      if (!"EXIT".equals(userInput.getCommandType()))
      {
        if ("STOR".equals(userInput.getCommandType()))
        {
          uploadFile(userInput.getCommandType());
        }
        else if ("RETR".equals(userInput.getCommandType()))
        {
          downloadFile(userInput.getCommandParam());
        }
        else if ("PASV".equals(userInput.getCommandType()))
        {
          String rawResponse = ftpClient.setToPassive();

          if (rawResponse.startsWith("240"))
          {
            Response response = new Response(rawResponse);
            ftpSession.setServerDataPort(Integer.valueOf(response.getData()));
          }

          System.out.println(rawResponse);
        }
        else
        {
          sendCommand(input);
        }
      }
      else continueFlag = false;
    }
    while (continueFlag);
  }

  private void sendCommand(String input) throws IOException
  {
    System.out.println(ftpClient.sendCommand(input));
  }

  private void uploadFile(String directory) throws IOException
  {
    if (ftpSession.getServerDataPort() == null)
    {
      System.out.println("There is no active dataTransfer channel, you need to run [PASV] to create this.");
    }
    else
    {
      System.out.println(ftpClient.uploadFile(directory, ftpSession.getServerDataPort()));
    }
  }

  private void downloadFile(String file) throws IOException
  {
    if (ftpSession.getServerDataPort() == null)
    {
      System.out.println("There is no active dataTransfer channel, you need to run [PASV] to create this.");
    }
    else
    {
      System.out.println(ftpClient.retrieveFile(ftpSession.getClientWorkingDirectory(), file, ftpSession.getServerDataPort(), new StatusContainer(0)));
    }
  }
}
