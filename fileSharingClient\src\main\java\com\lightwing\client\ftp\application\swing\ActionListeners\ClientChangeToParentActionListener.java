package com.lightwing.client.ftp.application.swing.ActionListeners;

import com.lightwing.client.ftp.application.clientOperations.ChangeParentOperation;
import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.application.swing.view.ClientView;

import java.awt.event.ActionEvent;

public class ClientChangeToParentActionListener extends AbstractActionListener
{
  public ClientChangeToParentActionListener(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, ClientView clientView)
  {
    super(clientController, ftpSession, ftpClient, clientView);
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
    ChangeParentOperation changeParentOperation = new ChangeParentOperation(ftpSession);
    changeParentOperation.process();
    clientController.updateClientView();
  }
}
