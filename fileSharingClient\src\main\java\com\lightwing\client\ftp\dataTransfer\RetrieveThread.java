package com.lightwing.client.ftp.dataTransfer;

import com.lightwing.client.ftp.application.swing.view.progressBar.StatusContainer;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

public class RetrieveThread implements Runnable
{
  private File fileToRetrieve;
  private Socket dataSocket;
  private StatusContainer statusContainer;

  public RetrieveThread(File file, int port, StatusContainer statusContainer, String host) throws IOException
  {
    fileToRetrieve = file;
    dataSocket = new Socket(host, port);
    this.statusContainer = statusContainer;
  }

  public void run()
  {
    if (dataSocket != null)
    {
      try
      {
        FileOutputStream fileOutputStream = new FileOutputStream(fileToRetrieve);
        InputStream inputStream = dataSocket.getInputStream();

        byte[] inputFileByteArray = new byte[1024];
        int count, bytesRead = 0;

        while ((count = inputStream.read(inputFileByteArray)) > 0)
        {
          bytesRead += count;
          statusContainer.setBytesTransferred(bytesRead);
          System.out.println("Bytes read [" + bytesRead + "]" + " Percentage [" + statusContainer.getProgress() + "]");
          fileOutputStream.write(inputFileByteArray, 0, count);
        }

        statusContainer.setFinished();

        fileOutputStream.close();
        inputStream.close();
      }
      catch (IOException e)
      {
      }
    }
  }
}
