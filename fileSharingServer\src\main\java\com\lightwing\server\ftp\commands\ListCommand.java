package com.lightwing.server.ftp.commands;

import com.lightwing.server.ftp.models.FileWrapper;
import com.lightwing.server.ftp.session.FtpSession;
import com.lightwing.server.ftp.utils.Response;

import java.io.File;

public class ListCommand implements Command
{
  public static final String COMMAND = "LIST";
  public static final String FILE_LIST_SEPARATOR = ";";
  public static final String FILE_PARAMETER_SEPARATOR = ",";
  public static final String DIRECTORY_SEPARATOR = "\\";

  public boolean isMe(String commandType)
  {
    return COMMAND.equals(commandType);
  }

  public boolean canAccess(boolean loggedIn)
  {
    return loggedIn;
  }

  public String process(String commandParam, FtpSession ftpSession)
  {
    FileWrapper workingDirectory = ftpSession.getFileWrapper();

    try
    {
      File[] fileList = workingDirectory.getFile().listFiles();

      if (fileList != null)
      {
        return Response.FILE_LIST.setData(constructListString(fileList)).toString();
      }

      else
      {
        return Response.FILE_LIST_ERROR.toString();
      }
    }
    catch (SecurityException e)
    {
      return Response.FILE_PERMISSION_ERROR.toString();
    }
  }

  private String constructListString(File[] files)
  {
    StringBuilder stringBuilder = new StringBuilder();

    for (File file : files)
    {
      if (file.isDirectory())
      {
        stringBuilder.append(DIRECTORY_SEPARATOR);
      }
      stringBuilder.append(file.getName().replace(" ", "%20"));
      stringBuilder.append(FILE_PARAMETER_SEPARATOR);
      stringBuilder.append(file.length());
      stringBuilder.append(FILE_LIST_SEPARATOR);
    }

    return stringBuilder.toString();
  }
}
