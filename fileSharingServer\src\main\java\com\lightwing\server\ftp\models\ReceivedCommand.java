package com.lightwing.server.ftp.models;

public class ReceivedCommand
{
  public static final String COMMAND_PARAM_SEPARATOR = " ";
  private String rawReceivedLine;

  public ReceivedCommand(String rawReceivedLine)
  {
    this.rawReceivedLine = rawReceivedLine;
  }

  public String getCommandType()
  {
    int separatorIndex = rawReceivedLine.indexOf(COMMAND_PARAM_SEPARATOR);

    if (separatorIndex == -1)
    {
      return rawReceivedLine;
    }

    return rawReceivedLine.substring(0, separatorIndex);

  }

  public String getCommandParam()
  {
    int separatorIndex = rawReceivedLine.indexOf(COMMAND_PARAM_SEPARATOR);

    if (separatorIndex == -1)
    {
      return rawReceivedLine;
    }

    return rawReceivedLine.substring(separatorIndex + 1);
  }
}
