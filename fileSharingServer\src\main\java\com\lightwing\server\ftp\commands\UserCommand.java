package com.lightwing.server.ftp.commands;

import com.lightwing.server.ftp.session.FtpSession;
import com.lightwing.server.ftp.utils.Response;

public class UserCommand implements Command
{
  public static final String COMMAND = "USER";

  private String username;
  private String password;

  public UserCommand(String username, String password)
  {
    this.username = username;
    this.password = password;
  }

  public boolean isMe(String commandType)
  {
    return COMMAND.equals(commandType);
  }

  public boolean canAccess(boolean loggedIn)
  {
    return true;
  }

  public String process(String commandParam, FtpSession ftpSession)
  {
    String[] credentials = commandParam.split(",");

    if (credentials.length != 2)
    {
      return Response.USER_DETAILS_INVALID.setData(commandParam).toString();
    }
    else
    {
      if (credentials[0].equals(username) && credentials[1].equals(password))
      {
        ftpSession.setLoggedIn();
        return Response.USER_LOGGED_IN.setData(credentials[0]).toString();
      }
      else
      {
        return Response.USER_DETAILS_INCORRECT.setData(commandParam).toString();
      }
    }
  }
}