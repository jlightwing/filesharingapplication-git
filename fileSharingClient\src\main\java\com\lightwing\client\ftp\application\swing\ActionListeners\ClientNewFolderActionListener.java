package com.lightwing.client.ftp.application.swing.ActionListeners;

import com.lightwing.client.ftp.application.clientOperations.NewFolderOperation;
import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.application.swing.view.ClientView;

import java.awt.event.ActionEvent;

public class ClientNewFolderActionListener extends AbstractActionListener
{
  public ClientNewFolderActionListener(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, ClientView clientView)
  {
    super(clientController, ftpSession, ftpClient, clientView);
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
    NewFolderOperation newFolderOperation = new NewFolderOperation(ftpSession);
    String newFolderName = clientView.showInputBox("Folder name:");

    if (newFolderName != null && !newFolderName.isEmpty())
    {
      newFolderOperation.process(newFolderName);
      clientController.updateClientView();
    }
  }
}
