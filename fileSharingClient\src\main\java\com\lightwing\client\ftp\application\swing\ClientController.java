package com.lightwing.client.ftp.application.swing;

import com.lightwing.client.ftp.application.swing.ActionListeners.*;
import com.lightwing.client.ftp.model.*;
import com.lightwing.client.ftp.application.swing.view.ClientView;

import javax.swing.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.IOException;

public class ClientController
{
  private FtpClient ftpClient;
  private FtpSession ftpSession;
  private ClientView clientView;
  private String initialFtpDir;

  public ClientController(FtpClient ftpClient, FtpSession ftpSession, String initialFtpDir) throws IOException
  {
    this.ftpClient = ftpClient;
    this.ftpSession = ftpSession;
    this.initialFtpDir = initialFtpDir;
  }

  public void run() throws IOException
  {
    ftpSession.setServerWorkingDirectory(new Response(ftpClient.printWorkingDirectory()).getData());
    ftpSession.setClientWorkingDirectory(initialFtpDir);

    Response listResponse = new Response(ftpClient.listDirectory());
    ftpSession.setServerDirectoryList(listResponse.getData());
    initialiseClientView();

    clientView.getServerOpenButton().addActionListener(new ServerOpenDirectoryActionListener(this, ftpSession, ftpClient, clientView));
    clientView.getServerParentButton().addActionListener(new ServerChangeToParentActionListener(this, ftpSession, ftpClient, clientView));
    clientView.getServerNewFolderButton().addActionListener(new ServerNewFolderActionListener(this, ftpSession, ftpClient, clientView));
    clientView.getServerDeleteButton().addActionListener(new ServerDeleteActionListener(this, ftpSession, ftpClient, clientView));

    clientView.getClientOpenButton().addActionListener(new ClientOpenDirectoryActionListener(this, ftpSession, ftpClient, clientView));
    clientView.getClientParentButton().addActionListener(new ClientChangeToParentActionListener(this, ftpSession, ftpClient, clientView));
    clientView.getClientNewFolderButton().addActionListener(new ClientNewFolderActionListener(this, ftpSession, ftpClient, clientView));
    clientView.getClientDeleteButton().addActionListener(new ClientDeleteActionListener(this, ftpSession, ftpClient, clientView));

    clientView.getDownloadButton().addActionListener(new RetrieveActionListener(this, ftpSession, ftpClient, clientView));
    clientView.getUploadButton().addActionListener(new StoreActionListener(this, ftpSession, ftpClient, clientView));

    updateServerView();
    updateClientView();
  }

  private void initialiseClientView()
  {
    final JFrame frame = new JFrame("FTP Client");
    clientView = new ClientView(frame);
    clientView.doLayout();
    frame.setSize(700,500);
    frame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    frame.addWindowListener(new WindowAdapter()
    {
      @Override
      public void windowClosing(WindowEvent event)
      {
        cleanUpSockets(frame);
      }
    });
    frame.add(clientView);
    frame.setVisible(true);
  }

  private void cleanUpSockets(JFrame frame)
  {
    try
    {
      ftpClient.exit();
    }
    catch (IOException e)
    {
      e.printStackTrace();
    }
    frame.dispose();
    System.exit(0);
  }

  public void updateServerView()
  {
    clientView.getSeverFileList().setListData(ftpSession.getServerDirectoryList());
    clientView.getServerWorkingDirectory().setText(ftpSession.getServerWorkingDirectory());
  }

  public void updateClientView()
  {
    clientView.getClientFileList().setListData(ftpSession.getClientDirectoryList());
    clientView.getClientWorkingDirectory().setText(ftpSession.getClientWorkingDirectory());
  }
}
