package com.lightwing.client.ftp.dataTransfer;

import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.DirectoryListElement;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.model.Response;
import com.lightwing.client.ftp.application.swing.view.ClientView;
import com.lightwing.client.ftp.application.swing.view.progressBar.StatusContainer;

import javax.swing.*;
import java.io.IOException;


public class StoreSwingWorker extends SwingWorker<Void, Void>
{
  private ClientController clientController;
  private FtpSession ftpSession;
  private FtpClient ftpClient;
  private JFrame progressFrame;
  private DirectoryListElement targetFile;

  public StoreSwingWorker(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, JFrame progressFrame, DirectoryListElement targetFile)
  {
    this.clientController = clientController;
    this.ftpSession = ftpSession;
    this.ftpClient = ftpClient;
    this.progressFrame = progressFrame;
    this.targetFile = targetFile;
  }

  @Override
  protected Void doInBackground() throws Exception
  {
    try
    {
      StatusContainer statusContainer = new StatusContainer(targetFile.getSize());

      Response uploadResponse = new Response(ftpClient.uploadFile(ftpSession.getClientWorkingDirectory() + "/" + targetFile.getName(), ftpSession.getServerDataPort()));

      if (Response.SUCCESS.equals(uploadResponse.getCode()))
      {
        uploadFile(statusContainer);
      }
      else
      {
        JOptionPane.showMessageDialog(progressFrame, uploadResponse.getDescription());
      }

      updateServerDirList();
      returnToMainView();
    }
    catch (IOException e1)
    {
      JOptionPane.showMessageDialog(progressFrame, "Error sending request to server");
      returnToMainView();
    }

    return null;
  }

  private void uploadFile(StatusContainer statusContainer) throws IOException
  {
    Response statusResponse = new Response(ftpClient.status());

    while(Response.STATUS_IN_PROGRESS.equals(statusResponse.getCode()))
    {
      if (!statusResponse.getData().equals("#"))
      {
        statusContainer.setBytesTransferred(Integer.parseInt(statusResponse.getData()));
      }
      else
      {
        statusContainer.setBytesTransferred(0);
      }

      setProgress(statusContainer.getProgress());
      statusResponse = new Response(ftpClient.status());
    }

    if (Response.STATUS_SUCCESS.equals(statusResponse.getCode()))
    {
      JOptionPane.showMessageDialog(progressFrame, statusResponse.getDescription());
    }
    else if (Response.STATUS_FAILURE.equals(statusResponse.getCode()))
    {
      JOptionPane.showMessageDialog(progressFrame, statusResponse.getDescription());
    }
  }

  private void updateServerDirList() throws IOException
  {
    Response listResponse = new Response(ftpClient.listDirectory());

    if (Response.FILE_LIST.equals(listResponse.getCode()))
    {
      ftpSession.setServerDirectoryList(listResponse.getData());
    }
  }

  private void returnToMainView()
  {
    clientController.updateServerView();

    progressFrame.setVisible(false);
    progressFrame.dispose();
  }
}
