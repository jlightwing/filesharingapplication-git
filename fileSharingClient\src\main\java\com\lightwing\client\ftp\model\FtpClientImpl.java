package com.lightwing.client.ftp.model;

import com.lightwing.client.ftp.dataTransfer.RetrieveThread;
import com.lightwing.client.ftp.dataTransfer.StoreThread;
import com.lightwing.client.ftp.application.swing.view.progressBar.StatusContainer;

import java.io.*;
import java.net.Socket;

public class FtpClientImpl implements FtpClient
{
  public static final String EOF = "\r\n";
  public static final String STORE = "STOR";
  public static final String RETRIEVE = "RETR";
  public static final String CHANGE_TO_PARENT_DIRECTORY = "CDUP";
  public static final String COMMAND_SEPARATOR = " ";
  public static final String CHANGE_WORKING_DIRECTORY = "CWD";
  public static final String DELETE_FILE = "DELE";
  public static final String LIST_DIRECTORY = "LIST";
  public static final String MAKE_DIRECTORY = "MKD";
  public static final String SET_PASSIVE = "PASV";
  public static final String PRINT_WORKING_DIRECTORY = "PWD";
  public static final String REMOVE_DIRECTORY = "RMD";
  public static final String STATUS = "STATUS";
  public static final String LOGIN = "USER";
  public static final String EXIT_SESSION_COMMAND = "EXIT_FTP";

  private BufferedWriter bufferedWriter;
  private BufferedReader bufferedReader;
  private String host;

  public FtpClientImpl(Socket clientSocket, String host) throws IOException
  {
    bufferedWriter = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream(), "UTF-8"));
    bufferedReader = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    this.host = host;
  }

  public String uploadFile(String filePath, int port) throws IOException
  {
    File fileToUpload = new File(filePath);

    if (fileToUpload.exists() && fileToUpload.isFile())
    {
      String string = sendCommand(STORE + COMMAND_SEPARATOR + fileToUpload.getName());

      if (Response.SUCCESS.equals(new Response(string).getCode()))
      {
        new Thread(new StoreThread(fileToUpload, port, host)).start();
      }

      return string;
    }

    return "530 + " + filePath + " There is a problem with the file name supplied";
  }

  public String retrieveFile(String workingDirectory, String file, int port, StatusContainer statusContainer) throws IOException
  {
    File newFile = new File(workingDirectory + "\\" + file);
    newFile.createNewFile();

    String string = sendCommand(RETRIEVE + COMMAND_SEPARATOR + file);

    if (Response.SUCCESS.equals(new Response(string).getCode()))
    {
      RetrieveThread retrieveThread = new RetrieveThread(newFile, port, statusContainer, host);
      new Thread(retrieveThread).start();

      return string;
    }
    else
    {
      return string;
    }
  }

  public String changeToParentDirectory() throws IOException
  {
    return sendCommand(CHANGE_TO_PARENT_DIRECTORY + COMMAND_SEPARATOR);
  }

  public String changeWorkingDirectory(String directoryPath) throws IOException
  {
    return sendCommand(CHANGE_WORKING_DIRECTORY + COMMAND_SEPARATOR + directoryPath);
  }

  public String deleteFile(String filePath) throws IOException
  {
    return sendCommand(DELETE_FILE + COMMAND_SEPARATOR + filePath);
  }

  public String listDirectory() throws IOException
  {
    return sendCommand(LIST_DIRECTORY + COMMAND_SEPARATOR);
  }

  public String makeDirectory(String directoryPath) throws IOException
  {
    return sendCommand(MAKE_DIRECTORY + COMMAND_SEPARATOR + directoryPath);
  }

  public String setToPassive() throws IOException
  {
    return sendCommand(SET_PASSIVE + COMMAND_SEPARATOR);
  }

  public String printWorkingDirectory() throws IOException
  {
    return sendCommand(PRINT_WORKING_DIRECTORY + COMMAND_SEPARATOR);
  }

  public String removeDirectory(String directoryPath) throws IOException
  {
    return sendCommand(REMOVE_DIRECTORY + COMMAND_SEPARATOR + directoryPath);
  }

  public String status() throws IOException
  {
    return sendCommand(STATUS + COMMAND_SEPARATOR);
  }

  public String login(String username, String password) throws IOException
  {
    return sendCommand(LOGIN + COMMAND_SEPARATOR + username + "," + password);
  }

  public String sendCommand(String command) throws IOException
  {
    bufferedWriter.write(command + EOF);
    bufferedWriter.flush();

    String string = bufferedReader.readLine();

    return string;
  }

  public void exit() throws IOException
  {
    sendCommand(EXIT_SESSION_COMMAND);
    bufferedReader.close();
    bufferedWriter.close();
  }
}
