package com.lightwing.client.ftp.application.clientOperations;

import com.lightwing.client.ftp.model.FtpSession;

import java.io.File;

public class ChangeWorkingDirectoryOperation
{
  private FtpSession ftpSession;

  public ChangeWorkingDirectoryOperation(FtpSession ftpSession)
  {
    this.ftpSession = ftpSession;
  }

  public void process(String newDirectory)
  {
    String newPath = ftpSession.getClientWorkingDirectory() + newDirectory;
    File newFile = new File(newPath);

    if (newFile.exists() && newFile.isDirectory())
    {
      ftpSession.setClientWorkingDirectory(newFile.getPath());
    }
  }
}
