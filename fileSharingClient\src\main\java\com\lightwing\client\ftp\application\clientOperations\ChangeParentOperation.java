package com.lightwing.client.ftp.application.clientOperations;

import com.lightwing.client.ftp.model.FtpSession;

import java.io.File;

public class ChangeParentOperation
{
  private FtpSession ftpSession;

  public ChangeParentOperation(FtpSession ftpSession)
  {
    this.ftpSession = ftpSession;
  }

  public void process()
  {
    File currentDir = new File(ftpSession.getClientWorkingDirectory());

    if (currentDir.exists() && currentDir.isDirectory())
    {
      String parent = currentDir.getParent();

      if (parent != null)
      {
        ftpSession.setClientWorkingDirectory(parent);
      }
    }
  }
}
