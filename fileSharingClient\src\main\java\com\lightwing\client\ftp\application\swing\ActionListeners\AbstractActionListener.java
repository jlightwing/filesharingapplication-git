package com.lightwing.client.ftp.application.swing.ActionListeners;

import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.application.swing.view.ClientView;
import com.lightwing.client.ftp.model.Response;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public abstract class AbstractActionListener implements ActionListener
{
  protected ClientController clientController;
  protected FtpSession ftpSession;
  protected FtpClient ftpClient;
  protected ClientView clientView;

  protected AbstractActionListener(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, ClientView clientView)
  {
    this.clientController = clientController;
    this.ftpSession = ftpSession;
    this.ftpClient = ftpClient;
    this.clientView = clientView;
  }

  public abstract void actionPerformed(ActionEvent e);

  protected void updateView(Response workingDirResponse) throws IOException
  {
    if (Response.SUCCESS.equals(workingDirResponse.getCode()))
    {
      Response listResponse = new Response(ftpClient.listDirectory());

      if (Response.FILE_LIST.equals(listResponse.getCode()))
      {
        ftpSession.setServerDirectoryList(listResponse.getData());
        ftpSession.setServerWorkingDirectory(workingDirResponse.getData());

        clientController.updateServerView();
      }
      else
      {
        clientView.showMessageBox(listResponse.getDescription());
      }
    }
    else
    {
      clientView.showMessageBox(workingDirResponse.getDescription());
    }
  }

  protected boolean setupDataChannel() throws IOException
  {
    if (ftpSession.getServerDataPort() == null)
    {
      Response passiveResponse = new Response(ftpClient.setToPassive());

      if (Response.PASV_SUCCESS.equals(passiveResponse.getCode()))
      {
        ftpSession.setServerDataPort(Integer.valueOf(passiveResponse.getData()));
        return true;
      }
      else
      {
        return false;
      }
    }
    else
    {
      return true;
    }
  }
}
