package com.lightwing.server.ftp.commands;

import com.lightwing.server.ftp.models.FileWrapper;
import com.lightwing.server.ftp.session.FtpSession;
import com.lightwing.server.ftp.utils.Response;

import java.io.File;

public class RemoveDirectoryCommand implements Command
{
  public static final String COMMAND = "RMD";

  public boolean isMe(String commandType)
  {
    return COMMAND.equals(commandType);
  }

  public boolean canAccess(boolean loggedIn)
  {
    return loggedIn;
  }

  public String process(String commandParam, FtpSession ftpSession)
  {
    File directoryToDelete = new File(commandParam);
    FileWrapper workingDirectory = ftpSession.getFileWrapper();

    try
    {
      if (directoryToDelete.exists() && directoryToDelete.isDirectory() && !directoryToDelete.getPath().equals(workingDirectory.getFile().getPath()))
      {
        if (directoryToDelete.delete())
        {
          return Response.SUCCESS.setData(workingDirectory.getFile().getPath()).toString();
        }
      }

      return Response.DIRECTORY_ACTION_FAULT.setData(commandParam).toString();
    }
    catch (SecurityException e)
    {
      return Response.FILE_PERMISSION_ERROR.setData(commandParam).toString();
    }
  }
}
