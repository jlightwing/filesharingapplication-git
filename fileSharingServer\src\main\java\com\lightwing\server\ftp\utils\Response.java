package com.lightwing.server.ftp.utils;

public enum Response
{
  //General information
  SUCCESS(200, "Success"),
  COMMAND_NOT_FOUND(500, "Command not found"),
  UNKNOWN_ERROR(999, "There was an error, please try again"),

  //Passive command
  PASV_SUCCESS(200, "Passive data transfer channel opened"),
  PASV_FAIL(510, "Unable to open passive data transfer channel"),

  //Directory actions
  FILE_LIST(200, "Directory content"),
  DIRECTORY_ACTION_FAULT(510, "There is a problem with the directory supplied"),
  DIRECTORY_DOES_NOT_EXIST(520, "There is no such directory"),
  FILE_ACTION_FAULT(530, "There is a problem with the file name supplied"),
  FILE_LIST_ERROR(540, "Can not obtain directory list"),
  FILE_PERMISSION_ERROR(550, "Access is not permitted to this location"),

  //Data transfer actions
  DATA_CHANNEL_NOT_OPEN(510, "There is no dataTransfer channel open, send [PASV]"),
  DATA_CHANNEL_BUSY(520, "The dataTransfer channel is busy"),
  STATUS_IN_PROGRESS(210, "The Transfer is in progress"),
  STATUS_SUCCESS(220, "The transfer has succeeded"),
  STATUS_FAILURE(230, "The transfer has failed"),
  STATUS_NO_STATUS(240, "There is no status available"),

  //User management actions
  USER_NOT_LOGGED_IN(510, "User is not logged in"),
  USER_DETAILS_INVALID(520, "User details supplied are not in correct format"),
  USER_DETAILS_INCORRECT(530, "User details supplied do not match"),
  USER_LOGGED_IN(200, "User successfully logged in");

  private final int code;
  private String data;
  private final String description;

  Response(int code, String description)
  {
    this.code = code;
    this.data = "";
    this.description = description;
  }

  public Response setData(String data)
  {
    this.data = data;
    return this;
  }

  @Override
  public String toString()
  {
    return code + " " + (data.isEmpty() ? "#" : data)  + " " + (description.isEmpty() ? "#" : description);
  }
}