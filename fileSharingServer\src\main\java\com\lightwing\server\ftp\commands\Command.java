package com.lightwing.server.ftp.commands;

import com.lightwing.server.ftp.session.FtpSession;

import java.io.IOException;

public interface Command
{
  boolean isMe(String commandType);

  boolean canAccess(boolean loggedIn);

  String process(String commandParam, FtpSession ftpSession) throws IOException;
}
