package com.lightwing.client.ftp.application.swing.ActionListeners;


import com.lightwing.client.ftp.application.swing.AppContainer;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpClientImpl;
import com.lightwing.client.ftp.model.Response;
import com.lightwing.client.ftp.application.swing.view.ConnectView;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.Socket;

public class ConnectActionListener implements ActionListener
{
  private ConnectView connectView;

  private JLabel hostMessageLabel;
  private JLabel portMessageLabel;
  private JLabel usernameMessageLabel;
  private JLabel passwordMessageLabel;

  private JTextField hostTextField;
  private JTextField portTextField;
  private JTextField usernameTextField;
  private JPasswordField passwordTextField;

  private AppContainer appContainer;

  public ConnectActionListener(ConnectView connectView, AppContainer appContainer)
  {
    this.connectView = connectView;

    this.hostMessageLabel = connectView.getHostMessageLabel();
    this.portMessageLabel = connectView.getPortMessageLabel();
    this.usernameMessageLabel = connectView.getUsernameMessageLabel();
    this.passwordMessageLabel = connectView.getPasswordMessageLabel();

    this.hostTextField = connectView.getHostTextField();
    this.portTextField = connectView.getPortTextField();
    this.usernameTextField = connectView.getUsernameTextField();
    this.passwordTextField = connectView.getPasswordTextField();

    this.appContainer = appContainer;
  }

  public void actionPerformed(ActionEvent e)
  {
    String host = hostTextField.getText();
    String port = portTextField.getText();
    String username = usernameTextField.getText();
    String password = new String(passwordTextField.getPassword());

    if (validateFields(host, port, username, password))
    {
      FtpClient ftpClient;

      try
      {
        ftpClient = new FtpClientImpl(new Socket(host, Integer.valueOf(port)), host);

        try
        {
          Response loginResponse = new Response(ftpClient.login(username, password));
          this.connectView.showMessageBox(loginResponse.getDescription());

          if (!Response.USER_LOGGED_IN.equals(loginResponse.getCode()))
          {
            return;
          }
        }
        catch (IOException e1)
        {
          this.connectView.showMessageBox("Error sending login request");
          return;
        }
      }
      catch (IOException e1)
      {
        this.connectView.showMessageBox("Can not connect with [Host: " + host + ", Port: " + port + "]");
        return;
      }

      appContainer.setFtpClient(ftpClient);

      try
      {
        if (!appContainer.runClient())
        {
          connectView.showMessageBox("Error starting the FTP client");
        }
      }
      catch (IOException e1)
      {
        this.connectView.showMessageBox("Error opening FTP client");
      }
    }
  }

  private boolean validateFields(String host, String port, String username, String password)
  {
    boolean isValid = true;

    if (host.isEmpty())
    {
      isValid = false;
      hostMessageLabel.setText("Please enter a valid host");
    }
    else
    {
      hostMessageLabel.setText("");
    }
    if (port.isEmpty() || !port.matches("^\\d+$"))
    {
      isValid = false;
      portMessageLabel.setText("Please enter a valid port");
    }
    else
    {
      portMessageLabel.setText("");
    }
    if (username.isEmpty())
    {
      isValid = false;
      usernameMessageLabel.setText("Please enter a valid username");
    }
    else
    {
      usernameMessageLabel.setText("");
    }
    if (password.isEmpty())
    {
      isValid = false;
      passwordMessageLabel.setText("Please enter a valid password");
    }
    else
    {
      passwordMessageLabel.setText("");
    }

    return isValid;
  }
}
