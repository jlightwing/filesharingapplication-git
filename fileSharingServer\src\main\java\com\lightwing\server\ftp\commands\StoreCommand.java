package com.lightwing.server.ftp.commands;

import com.lightwing.server.ftp.dataChannel.DataChannel;
import com.lightwing.server.ftp.dataChannel.DataChannelStatus;
import com.lightwing.server.ftp.session.FtpSession;
import com.lightwing.server.ftp.dataChannel.StoreThread;
import com.lightwing.server.ftp.utils.Response;

import java.io.*;


public class StoreCommand implements Command
{
  public static final String COMMAND = "STOR";
  public static final String FILE_SEPARATOR = "/";

  public boolean isMe(String commandType)
  {
    return COMMAND.equals(commandType);
  }

  public boolean canAccess(boolean loggedIn)
  {
    return loggedIn;
  }

  public String process(String commandParam, FtpSession ftpSession) throws IOException
  {
    DataChannel dataChannel = ftpSession.getDataChannel();
    DataChannelStatus dataChannelStatus = dataChannel.getStatus();

    if (dataChannelStatus.equals(DataChannelStatus.IDLE))
    {
      File newFile = new File(ftpSession.getFileWrapper().getFile().getPath() + FILE_SEPARATOR + commandParam);
      newFile.createNewFile();

      dataChannel.transferStarted();
      StoreThread storeThread = new StoreThread(dataChannel, newFile);
      new Thread(storeThread).start();

      return Response.SUCCESS.toString();
    }
    else if (dataChannelStatus.equals(DataChannelStatus.IN_PROGRESS))
    {
      return Response.DATA_CHANNEL_BUSY.setData(dataChannelStatus.toString()).toString();
    }
    else if (dataChannelStatus.equals(DataChannelStatus.NO_SOCKET))
    {
      return Response.DATA_CHANNEL_NOT_OPEN.setData(dataChannelStatus.toString()).toString();
    }
    else
    {
      return Response.UNKNOWN_ERROR.toString();
    }
  }
}
