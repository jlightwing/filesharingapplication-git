package com.lightwing.server.ftp.commands;

import com.lightwing.server.ftp.session.FtpSession;
import com.lightwing.server.ftp.utils.Response;

public class PrintWorkingDirectoryCommand implements Command
{
  public static final String COMMAND = "PWD";

  public boolean isMe(String commandType)
  {
    return COMMAND.equals(commandType);
  }

  public boolean canAccess(boolean loggedIn)
  {
    return loggedIn;
  }

  public String process(String commandParam, FtpSession ftpSession)
  {
    try
    {
      return Response.SUCCESS.setData(ftpSession.getFileWrapper().getFile().getPath()).toString();
    }
    catch (SecurityException e)
    {
      return Response.FILE_PERMISSION_ERROR.toString();
    }
  }
}
