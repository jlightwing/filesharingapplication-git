package com.lightwing.client.ftp.model;


import java.io.File;
import java.io.IOException;

public class FtpSession
{
  public static final String FILE_LIST_SEPARATOR = ";";
  public static final String FILE_PARAMETER_SEPARATOR = ",";
  public static final String DIRECTORY_SEPARATOR = "\\";

  private String serverWorkingDirectory;
  private String clientWorkingDirectory;
  private String serverDirectoryList;
  private Integer serverDataPort;

  public FtpSession()
  {
    this.serverWorkingDirectory = "";
    this.serverDirectoryList = "";
    this.clientWorkingDirectory = "";
    this.serverDataPort = null;
  }

  public void setServerDataPort(Integer serverDataPort)
  {
    this.serverDataPort = serverDataPort;
  }

  public void setServerWorkingDirectory(String serverWorkingDirectory)
  {
    this.serverWorkingDirectory = serverWorkingDirectory;
  }

  public void setServerDirectoryList(String serverDirectoryList)
  {
    this.serverDirectoryList = serverDirectoryList;
  }

  public Integer getServerDataPort()
  {
    return serverDataPort;
  }

  public String getServerWorkingDirectory()
  {
    return serverWorkingDirectory;
  }

  public String getClientWorkingDirectory()
  {
    return clientWorkingDirectory;
  }

  public void setClientWorkingDirectory(String clientWorkingDirectory)
  {
    this.clientWorkingDirectory = clientWorkingDirectory;
  }

  public DirectoryListElement[] getServerDirectoryList()
  {
    return getDirectoryListElements(serverDirectoryList);
  }

  public DirectoryListElement[] getClientDirectoryList()
  {
    String clientDirectoryList = constructListString(new File(clientWorkingDirectory).listFiles());

    return getDirectoryListElements(clientDirectoryList);
  }

  private DirectoryListElement[] getDirectoryListElements(String directoryList)
  {
    if (!directoryList.isEmpty())
    {
      String[] directoryContents = directoryList.split(";");
      DirectoryListElement[] directoryListElements = new DirectoryListElement[directoryContents.length];

      for (int x = 0; x < directoryContents.length; x++)
      {
        directoryListElements[x] = new DirectoryListElement(directoryContents[x]);
      }

      return directoryListElements;
    }
    else
    {
      return new DirectoryListElement[0];
    }
  }

  private String constructListString(File[] files)
  {
    StringBuilder stringBuilder = new StringBuilder();

    for (File file : files)
    {
      if (file.isDirectory())
      {
        stringBuilder.append(DIRECTORY_SEPARATOR);
      }
      stringBuilder.append(file.getName().replace(" ", "%20"));
      stringBuilder.append(FILE_PARAMETER_SEPARATOR);
      stringBuilder.append(file.length());
      stringBuilder.append(FILE_LIST_SEPARATOR);
    }

    return stringBuilder.toString();
  }
}
