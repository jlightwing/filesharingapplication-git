package com.lightwing.server.ftp.dataChannel;

import java.io.*;
import java.net.ServerSocket;

public class RetrieveThread implements Runnable
{
  private DataChannel dataChannel;
  private File fileToUpload;

  public RetrieveThread(DataChannel dataChannel, File fileToUpload)
  {
    this.dataChannel = dataChannel;
    this.fileToUpload = fileToUpload;
  }

  public void run()
  {
    ServerSocket dataSocket = dataChannel.getDataSocket();

    if (dataSocket != null)
    {
      try
      {
        OutputStream outputStream = dataSocket.accept().getOutputStream();
        FileInputStream fileInputStream = new FileInputStream(fileToUpload);

        byte[] fileByteArray = new byte[(int) fileToUpload.length()];

        fileInputStream.read(fileByteArray);
        outputStream.write(fileByteArray);

        outputStream.flush();
        outputStream.close();

        fileInputStream.close();
        dataChannel.incrementBytesTransferred(fileToUpload.length());
        dataChannel.transferEnded(PreviousTransferStatus.SUCCESS);
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
  }
}