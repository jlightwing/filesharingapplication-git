package com.lightwing.server.ftp.dataChannel;

public enum DataChannelStatus
{
  NO_SOCKET("NO_SOCKET"),
  IDLE("IDLE"),
  IN_PROGRESS("IN_PROGRESS");

  private String code;

  DataChannelStatus(String code)
  {
    this.code = code;
  }

  @Override
  public String toString()
  {
    return code;
  }
}
