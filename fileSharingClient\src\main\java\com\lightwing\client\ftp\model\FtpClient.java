package com.lightwing.client.ftp.model;

import com.lightwing.client.ftp.application.swing.view.progressBar.StatusContainer;

import java.io.IOException;

public interface FtpClient
{
  String uploadFile(String filePath, int port) throws IOException;

  String retrieveFile(String workingDirectory, String file, int port, StatusContainer statusContainer) throws IOException;

  String changeToParentDirectory() throws IOException;

  String changeWorkingDirectory(String directoryPath) throws IOException;

  String deleteFile(String filePath) throws IOException;

  String listDirectory() throws IOException;

  String makeDirectory(String directoryPath) throws IOException;

  String setToPassive() throws IOException;

  String printWorkingDirectory() throws IOException;

  String removeDirectory(String directoryPath) throws IOException;

  String status() throws IOException;

  String login(String username, String password) throws IOException;

  String sendCommand(String command) throws IOException;

  void exit() throws IOException;
}
