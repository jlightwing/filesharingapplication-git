package com.lightwing.client.ftp.application.swing.ActionListeners;

import com.lightwing.client.ftp.application.clientOperations.ChangeWorkingDirectoryOperation;
import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.DirectoryListElement;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.application.swing.view.ClientView;

import java.awt.event.ActionEvent;

public class ClientOpenDirectoryActionListener extends AbstractActionListener
{
  public ClientOpenDirectoryActionListener(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, ClientView clientView)
  {
    super(clientController, ftpSession, ftpClient, clientView);
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
    if (clientView.getClientFileList().getSelectedValue() instanceof DirectoryListElement)
    {
      DirectoryListElement targetDirectory = (DirectoryListElement) clientView.getClientFileList().getSelectedValue();

      if (targetDirectory.isDirectory())
      {
        ChangeWorkingDirectoryOperation changeWorkingDirectoryOperation = new ChangeWorkingDirectoryOperation(ftpSession);
        changeWorkingDirectoryOperation.process(targetDirectory.getName());
        clientController.updateClientView();
      }
      else
      {
        clientView.showMessageBox("Not a directory");
      }
    }
    else
    {
      clientView.showMessageBox("Open error");
    }
  }
}
