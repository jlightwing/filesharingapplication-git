package com.lightwing.client.ftp.application.swing.ActionListeners;

import com.lightwing.client.ftp.application.clientOperations.DeleteOperation;
import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.DirectoryListElement;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.application.swing.view.ClientView;

import java.awt.event.ActionEvent;

public class ClientDeleteActionListener extends AbstractActionListener
{
  public ClientDeleteActionListener(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, ClientView clientView)
  {
    super(clientController, ftpSession, ftpClient, clientView);
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
    DeleteOperation deleteOperation = new DeleteOperation(ftpSession);

    if (clientView.getClientFileList().getSelectedValue() instanceof DirectoryListElement)
    {
      DirectoryListElement target = (DirectoryListElement) clientView.getClientFileList().getSelectedValue();
      deleteOperation.process(ftpSession.getClientWorkingDirectory() + "\\" + target.getName());
      clientController.updateClientView();
    }
  }
}
