package com.lightwing.client.ftp.application.clientOperations;

import com.lightwing.client.ftp.model.FtpSession;

import java.io.File;

public class DeleteOperation
{
  private FtpSession ftpSession;

  public DeleteOperation(FtpSession ftpSession)
  {
    this.ftpSession = ftpSession;
  }

  public void process(String fileToDeletePath)
  {
    File fileToDelete = new File(fileToDeletePath);

    if (fileToDelete.exists())
    {
      fileToDelete.delete();
    }
  }
}
