package com.lightwing.client.ftp.application.swing.view;


import javax.swing.*;
import java.awt.*;

public class ConnectView extends JPanel
{
  private JLabel hostLabel;
  private JTextField hostTextField;
  private JLabel hostMessageLabel;
  private JLabel portLabel;
  private JTextField portTextField;
  private JLabel portMessageLabel;
  private JLabel usernameLabel;
  private JTextField usernameTextField;
  private JLabel usernameMessageLabel;
  private JLabel passwordLabel;
  private JPasswordField passwordTextField;
  private JLabel passwordMessageField;
  private JButton connectButton;

  public ConnectView()
  {
    initComponents();
  }

  private void initComponents()
  {
    hostLabel = new JLabel();
    hostTextField = new JTextField();
    hostMessageLabel = new JLabel();
    portLabel = new JLabel();
    portTextField = new JTextField();
    portMessageLabel = new JLabel();
    usernameLabel = new JLabel();
    usernameTextField = new JTextField();
    usernameMessageLabel = new JLabel();
    passwordLabel = new JLabel();
    passwordTextField = new JPasswordField();
    passwordMessageField = new JLabel();
    connectButton = new JButton();

    setLayout(new GridBagLayout());
    ((GridBagLayout) getLayout()).columnWidths = new int[]{73, 163, 0, 0};
    ((GridBagLayout) getLayout()).rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0};
    ((GridBagLayout) getLayout()).columnWeights = new double[]{0.0, 0.0, 0.0, 1.0E-4};
    ((GridBagLayout) getLayout()).rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0E-4};

    //---- hostLabel ----
    hostLabel.setText("Host");
    add(hostLabel, new GridBagConstraints(0, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
    add(hostTextField, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

    //---- hostMessageLabel ----
    hostMessageLabel.setForeground(Color.red);
    add(hostMessageLabel, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

    //---- portLabel ----
    portLabel.setText("Port");
    add(portLabel, new GridBagConstraints(0, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
    add(portTextField, new GridBagConstraints(1, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

    //---- portMessageLabel ----
    portMessageLabel.setForeground(Color.red);
    add(portMessageLabel, new GridBagConstraints(2, 1, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

    //---- usernameLabel ----
    usernameLabel.setText("Username");
    add(usernameLabel, new GridBagConstraints(0, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
    add(usernameTextField, new GridBagConstraints(1, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

    //---- usernameMessageLabel ----
    usernameMessageLabel.setForeground(Color.red);
    add(usernameMessageLabel, new GridBagConstraints(2, 2, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

    //---- passwordLabel ----
    passwordLabel.setText("Password");
    add(passwordLabel, new GridBagConstraints(0, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
    add(passwordTextField, new GridBagConstraints(1, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));

    //---- passwordMessageField ----
    passwordMessageField.setForeground(Color.red);
    add(passwordMessageField, new GridBagConstraints(2, 3, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 0), 0, 0));

    //---- connectButton ----
    connectButton.setText("Connect");
    add(connectButton, new GridBagConstraints(1, 5, 1, 1, 0.0, 0.0,
            GridBagConstraints.CENTER, GridBagConstraints.BOTH,
            new Insets(0, 0, 5, 5), 0, 0));
  }

  public void showMessageBox(String message)
  {
    JOptionPane.showMessageDialog(this, message);
  }

  public JLabel getHostLabel()
  {
    return hostLabel;
  }

  public void setHostLabel(JLabel hostLabel)
  {
    this.hostLabel = hostLabel;
  }

  public JTextField getHostTextField()
  {
    return hostTextField;
  }

  public void setHostTextField(JTextField hostTextField)
  {
    this.hostTextField = hostTextField;
  }

  public JLabel getHostMessageLabel()
  {
    return hostMessageLabel;
  }

  public void setHostMessageLabel(JLabel hostMessageLabel)
  {
    this.hostMessageLabel = hostMessageLabel;
  }

  public JLabel getPortLabel()
  {
    return portLabel;
  }

  public void setPortLabel(JLabel portLabel)
  {
    this.portLabel = portLabel;
  }

  public JTextField getPortTextField()
  {
    return portTextField;
  }

  public void setPortTextField(JTextField portTextField)
  {
    this.portTextField = portTextField;
  }

  public JLabel getPortMessageLabel()
  {
    return portMessageLabel;
  }

  public void setPortMessageLabel(JLabel portMessageLabel)
  {
    this.portMessageLabel = portMessageLabel;
  }

  public JLabel getUsernameLabel()
  {
    return usernameLabel;
  }

  public void setUsernameLabel(JLabel usernameLabel)
  {
    this.usernameLabel = usernameLabel;
  }

  public JTextField getUsernameTextField()
  {
    return usernameTextField;
  }

  public void setUsernameTextField(JTextField usernameTextField)
  {
    this.usernameTextField = usernameTextField;
  }

  public JLabel getUsernameMessageLabel()
  {
    return usernameMessageLabel;
  }

  public void setUsernameMessageLabel(JLabel usernameMessageLabel)
  {
    this.usernameMessageLabel = usernameMessageLabel;
  }

  public JLabel getPasswordLabel()
  {
    return passwordLabel;
  }

  public void setPasswordLabel(JLabel passwordLabel)
  {
    this.passwordLabel = passwordLabel;
  }

  public JPasswordField getPasswordTextField()
  {
    return passwordTextField;
  }

  public void setPasswordTextField(JPasswordField passwordTextField)
  {
    this.passwordTextField = passwordTextField;
  }

  public JLabel getPasswordMessageLabel()
  {
    return passwordMessageField;
  }

  public void setPasswordMessageField(JLabel passwordMessageField)
  {
    this.passwordMessageField = passwordMessageField;
  }

  public JButton getConnectButton()
  {
    return connectButton;
  }

  public void setConnectButton(JButton connectButton)
  {
    this.connectButton = connectButton;
  }
}
