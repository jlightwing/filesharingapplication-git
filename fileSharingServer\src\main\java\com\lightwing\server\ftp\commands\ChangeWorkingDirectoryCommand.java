package com.lightwing.server.ftp.commands;

import com.lightwing.server.ftp.models.FileWrapper;
import com.lightwing.server.ftp.session.FtpSession;
import com.lightwing.server.ftp.utils.Response;

public class ChangeWorkingDirectoryCommand implements Command
{
  public static final String COMMAND = "CWD";

  public boolean isMe(String commandType)
  {
    return COMMAND.equals(commandType);
  }

  public boolean canAccess(boolean loggedIn)
  {
    return loggedIn;
  }

  public String process(String commandParam, FtpSession ftpSession)
  {
    FileWrapper workingDirectory = ftpSession.getFileWrapper();

    try
    {
      if (workingDirectory.changeWorkingDirectory(commandParam))
      {
        return Response.SUCCESS.setData(workingDirectory.getFile().getPath()).toString();
      }
      else
      {
        return Response.DIRECTORY_DOES_NOT_EXIST.setData(commandParam).toString();
      }
    }
    catch (SecurityException e)
    {
      return Response.FILE_PERMISSION_ERROR.setData(commandParam).toString();
    }
  }
}
