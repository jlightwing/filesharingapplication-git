package com.lightwing.server.ftp.models;

import java.io.File;

public class FileWrapper
{
  private File file;

  public FileWrapper(File file)
  {
    this.file = file;
  }

  public File getFile()
  {
    return file;
  }

  public boolean changeWorkingDirectory(String path)
  {
    File newFile = new File(path);

    if (newFile.exists())
    {
      file = newFile;
      return true;
    }
    else
    {
      return false;
    }
  }
}
