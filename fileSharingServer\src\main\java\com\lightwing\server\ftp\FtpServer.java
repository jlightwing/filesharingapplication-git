package com.lightwing.server.ftp;

import java.io.IOException;

public interface FtpServer
{
  void run() throws IOException;
}
