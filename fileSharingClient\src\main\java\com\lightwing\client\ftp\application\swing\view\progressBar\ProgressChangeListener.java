package com.lightwing.client.ftp.application.swing.view.progressBar;

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;


public class ProgressChangeListener implements PropertyChangeListener
{
  private JFrame parentFrame;
  private JFrame progressFrame;
  private JProgressBar progressBar;

  public ProgressChangeListener(JFrame frame)
  {
    this.parentFrame = frame;
    this.progressBar = makeProgressBar();
  }

  public void propertyChange(PropertyChangeEvent evt)
  {
    if ("progress" == evt.getPropertyName())
    {
      int progress = (Integer) evt.getNewValue();

      progressBar.setValue(progress);
    }
  }

  public JFrame getProgressFrame()
  {
    return progressFrame;
  }

  private JProgressBar makeProgressBar()
  {
    progressFrame = new JFrame("Progress");
    progressFrame.setLocationRelativeTo(parentFrame);
    progressFrame.setSize(300, 75);
    progressFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

    JProgressBar progressBar = new JProgressBar(0, 100);
    progressBar.setValue(0);
    progressBar.setStringPainted(true);

    JPanel panel = new JPanel();
    panel.add(BorderLayout.CENTER, progressBar);

    progressFrame.add(panel);
    progressFrame.setVisible(true);

    return progressBar;
  }
}
