package com.lightwing.client.ftp.model;

public class DirectoryListElement
{
  private String name;
  private long size;

  private boolean isDirectory;

  public DirectoryListElement (String directoryElementRaw)
  {
    String[] directoryElementSplit = directoryElementRaw.split(",");

    if (directoryElementSplit .length > 1)
    {
      name = directoryElementSplit[0].replace("%20", " ");
      size = Long.valueOf(directoryElementSplit[1]);
      isDirectory = directoryElementSplit[0].startsWith("\\");
    }
  }

  public boolean isDirectory()
  {
    return isDirectory;
  }

  public String getName()
  {
    return name;
  }

  public long getSize()
  {
    return size;
  }

  @Override
  public String toString()
  {
    return getName() + (isDirectory() ? "" : "......." + getSize() + " Bytes");
  }
}
