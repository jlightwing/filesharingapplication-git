package com.lightwing.server.ftp.dataChannel;


import java.io.IOException;
import java.net.ServerSocket;

public class DataChannel
{
  private ServerSocket dataSocket;
  private DataChannelStatus dataChannelStatus;
  private PreviousTransferStatus previousTransferStatus;

  long bytesTransferred = 0;

  public DataChannel()
  {
    dataChannelStatus = DataChannelStatus.NO_SOCKET;
    previousTransferStatus = PreviousTransferStatus.NO_TRANSFER;
  }

  public int initialiseDataSocket() throws IOException
  {
    if (dataSocket == null)
    {
      dataSocket = new ServerSocket(0);
      dataChannelStatus = DataChannelStatus.IDLE;
    }

    return dataSocket.getLocalPort();
  }

  public DataChannelStatus getStatus()
  {
    return dataChannelStatus;
  }

  public ServerSocket getDataSocket()
  {
    return dataSocket;
  }

  public void transferStarted()
  {
    dataChannelStatus = DataChannelStatus.IN_PROGRESS;
    bytesTransferred = 0;
  }

  public void transferEnded(PreviousTransferStatus transferStatus)
  {
    previousTransferStatus = transferStatus;
    dataChannelStatus = DataChannelStatus.IDLE;
  }

  public void incrementBytesTransferred(long bytesTransferred)
  {
    this.bytesTransferred += bytesTransferred;
  }

  public long getBytesTransferred()
  {
    return bytesTransferred;
  }

  public PreviousTransferStatus getPreviousTransferStatus()
  {
    return previousTransferStatus;
  }
}
