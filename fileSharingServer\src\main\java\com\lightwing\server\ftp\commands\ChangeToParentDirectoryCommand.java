package com.lightwing.server.ftp.commands;

import com.lightwing.server.ftp.models.FileWrapper;
import com.lightwing.server.ftp.session.FtpSession;
import com.lightwing.server.ftp.utils.Response;

public class ChangeToParentDirectoryCommand implements Command
{
  public static final String COMMAND = "CDUP";

  public boolean isMe(String commandType)
  {
    return COMMAND.equals(commandType);
  }

  public boolean canAccess(boolean loggedIn)
  {
    return loggedIn;
  }

  public String process(String commandParam, FtpSession ftpSession)
  {
    FileWrapper workingDirectory = ftpSession.getFileWrapper();

    try
    {
      if (workingDirectory.getFile().getParent() != null)
      {
        workingDirectory.changeWorkingDirectory(workingDirectory.getFile().getParent());
      }

      return Response.SUCCESS.setData(workingDirectory.getFile().getPath()).toString();
    }
    catch (SecurityException e)
    {
      return Response.FILE_PERMISSION_ERROR.setData(workingDirectory.getFile().getParent()).toString();
    }
  }
}
