package com.lightwing.client.ftp.application.swing;


import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;

import javax.swing.*;
import java.io.File;
import java.io.IOException;

public class AppContainer
{
  private ClientController clientController;
  private ConnectController connectController;
  private FtpClient ftpClient;
  private FtpSession ftpSession;
  private String initialFtpDir;

  private static final String CLIENT_BASE_DIRECTORY = "/ftp/client";

  public AppContainer()
  {
    setupLookAndFeel();
    connectController = new ConnectController(this);
  }

  private void setupLookAndFeel()
  {
    try
    {
      UIManager.setLookAndFeel(
              UIManager.getSystemLookAndFeelClassName());
    }
    catch (ClassNotFoundException e)
    {
      e.printStackTrace();
    }
    catch (InstantiationException e)
    {
      e.printStackTrace();
    }
    catch (IllegalAccessException e)
    {
      e.printStackTrace();
    }
    catch (UnsupportedLookAndFeelException e)
    {
      e.printStackTrace();
    }
  }

  public void setFtpClient(FtpClient ftpClient)
  {
    this.ftpClient = ftpClient;
  }

  public boolean runClient() throws IOException
  {
    if (ftpClient != null && setupFiles())
    {
      ftpSession = new FtpSession();
      clientController = new ClientController(ftpClient, ftpSession, initialFtpDir);
      clientController.run();

      connectController.destroySelf();
      return true;
    }
    else
    {
      return false;
    }
  }

  private boolean setupFiles()
  {
    String homeDir = System.getProperty("user.home");
    File baseDirectory = new File(homeDir + CLIENT_BASE_DIRECTORY);

    if (!baseDirectory.exists())
    {
      baseDirectory.mkdirs();
      initialFtpDir = baseDirectory.getPath();
      return true;
    }
    else if (baseDirectory.exists() && baseDirectory.isDirectory())
    {
      initialFtpDir = baseDirectory.getPath();
      return true;
    }
    else
    {
      return false;
    }
  }
}
