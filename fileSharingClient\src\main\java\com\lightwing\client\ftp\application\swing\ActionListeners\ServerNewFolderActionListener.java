package com.lightwing.client.ftp.application.swing.ActionListeners;

import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.model.Response;
import com.lightwing.client.ftp.application.swing.view.ClientView;

import java.awt.event.ActionEvent;
import java.io.IOException;


public class ServerNewFolderActionListener extends AbstractActionListener
{
  public ServerNewFolderActionListener(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, ClientView clientView)
  {
    super(clientController, ftpSession, ftpClient, clientView);
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
    try
    {
      String newFolderName = clientView.showInputBox("Folder name:");

      if (newFolderName != null && !newFolderName.isEmpty())
      {
        Response makeDirectoryResponse = new Response(ftpClient.makeDirectory(ftpSession.getServerWorkingDirectory() + "/" + newFolderName));
        updateView(makeDirectoryResponse);
      }
    }
    catch (IOException e1)
    {
      clientView.showMessageBox("Error sending request to server");
    }
  }
}
