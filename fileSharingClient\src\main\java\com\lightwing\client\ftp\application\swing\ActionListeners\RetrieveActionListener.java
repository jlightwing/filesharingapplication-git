package com.lightwing.client.ftp.application.swing.ActionListeners;

import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.DirectoryListElement;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.application.swing.view.ClientView;
import com.lightwing.client.ftp.application.swing.view.progressBar.ProgressChangeListener;
import com.lightwing.client.ftp.dataTransfer.RetrieveSwingWorker;

import java.awt.event.ActionEvent;
import java.io.IOException;

public class RetrieveActionListener extends AbstractActionListener
{
  public RetrieveActionListener(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, ClientView clientView)
  {
    super(clientController, ftpSession, ftpClient, clientView);
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
    try
    {
      if (clientView.getSeverFileList().getSelectedValue() instanceof DirectoryListElement)
      {
        retrieveFile();
      }
      else
      {
        clientView.showMessageBox("Invalid item selected");
      }
    }
    catch (IOException e1)
    {
      clientView.showMessageBox("Error sending request to server");
    }
  }

  private void retrieveFile() throws IOException
  {
    DirectoryListElement targetFile = (DirectoryListElement) clientView.getSeverFileList().getSelectedValue();

    if (!targetFile.isDirectory())
    {
      if (setupDataChannel())
      {
        ProgressChangeListener progressChangeListener = new ProgressChangeListener(clientView.getFrame());
        RetrieveSwingWorker retrieveSwingWorker = new RetrieveSwingWorker(clientController, ftpSession, ftpClient, progressChangeListener.getProgressFrame(), targetFile);
        retrieveSwingWorker.addPropertyChangeListener(progressChangeListener);
        retrieveSwingWorker.execute();
      }
      else
      {
        clientView.showMessageBox("Data channel could not be opened");
      }
    }
    else
    {
      clientView.showMessageBox("A file is not selected");
    }
  }
}
