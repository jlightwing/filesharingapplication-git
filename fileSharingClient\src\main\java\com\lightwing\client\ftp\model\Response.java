package com.lightwing.client.ftp.model;

public class Response
{
  //General information
  public static final String SUCCESS = "200";
  public static final String COMMAND_NOT_FOUND ="500";
  public static final String UNKNOWN_ERROR = "999";

  //Passive command
  public static final String PASV_SUCCESS = "200";
  public static final String PASV_FAIL = "510";

  //Directory actions
  public static final String FILE_LIST = "200";
  public static final String DIRECTORY_ACTION_FAULT = "510";
  public static final String DIRECTORY_DOES_NOT_EXIST = "520";
  public static final String FILE_ACTION_FAULT = "530";
  public static final String FILE_LIST_ERROR = "540";
  public static final String FILE_PERMISSION_ERROR = "550";

  //Data transfer actions
  public static final String DATA_CHANNEL_NOT_OPEN = "510";
  public static final String DATA_CHANNEL_BUSY = "520";
  public static final String STATUS_IN_PROGRESS = "210";
  public static final String STATUS_SUCCESS = "220";
  public static final String STATUS_FAILURE = "230";
  public static final String STATUS_NO_STATUS = "240";

  //User management actions
  public static final String USER_NOT_LOGGED_IN = "510";
  public static final String USER_DETAILS_INVALID = "520";
  public static final String USER_DETAILS_INCORRECT = "530";
  public static final String USER_LOGGED_IN = "200";

  private String response;

  public Response(String response)
  {
    this.response = response;
  }

  public boolean isValid()
  {
    return response.split(" ").length > 2;
  }

  public String getCode()
  {
    String[] splitStrings = response.split(" ");

    if (splitStrings.length > 0)
    {
      return splitStrings[0];
    }

    return "";
  }

  public String getData()
  {
    String[] splitStrings = response.split(" ");

    if (splitStrings.length >  1 && !splitStrings[1].equals("#"))
    {
      return splitStrings[1];
    }

    return "";
  }

  public String getDescription()
  {
    int firstSpace = response.indexOf(" ");
    int secondSpace = response.indexOf(" ", firstSpace + 1);
    return response.substring(secondSpace + 1);
  }
}
