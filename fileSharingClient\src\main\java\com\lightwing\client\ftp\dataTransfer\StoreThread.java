package com.lightwing.client.ftp.dataTransfer;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

public class StoreThread implements Runnable
{
  private Socket dataSocket;
  private File fileToUpload;

  public StoreThread(File file, int port, String host) throws IOException
  {
    fileToUpload = file;
    dataSocket = new Socket(host, port);
  }

  public void run()
  {
    try
    {
      FileInputStream fileInputStream = new FileInputStream(fileToUpload);
      OutputStream outputStream = dataSocket.getOutputStream();
      byte[] fileByteArray = new byte[(int)fileToUpload.length()];

      fileInputStream.read(fileByteArray);
      outputStream.write(fileByteArray);
      outputStream.flush();
      outputStream.close();
      fileInputStream.close();
    }
    catch (IOException e)
    {
    }
  }
}
