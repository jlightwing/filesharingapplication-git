package com.lightwing.client.ftp.application.swing.ActionListeners;

import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.DirectoryListElement;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.model.Response;
import com.lightwing.client.ftp.application.swing.view.ClientView;

import java.awt.event.ActionEvent;
import java.io.IOException;

public class ServerOpenDirectoryActionListener extends AbstractActionListener
{
  public ServerOpenDirectoryActionListener(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, ClientView clientView)
  {
    super(clientController, ftpSession, ftpClient, clientView);
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
    try
    {
      if (clientView.getSeverFileList().getSelectedValue() instanceof DirectoryListElement)
      {
        DirectoryListElement targetDirectory = (DirectoryListElement)clientView.getSeverFileList().getSelectedValue();

        if (targetDirectory.isDirectory())
        {
          Response response = new Response(ftpClient.changeWorkingDirectory(ftpSession.getServerWorkingDirectory() + "/" + targetDirectory.getName()));
          updateView(response);
        }
        else
        {
          clientView.showMessageBox("A directory is not selected");
        }
      }
      else
      {
        clientView.showMessageBox("Invalid item selected");
      }
    }
    catch (IOException e1)
    {
      clientView.showMessageBox("Error sending request to server");
    }
  }
}