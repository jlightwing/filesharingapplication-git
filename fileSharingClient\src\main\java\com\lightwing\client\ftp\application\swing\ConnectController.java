package com.lightwing.client.ftp.application.swing;


import com.lightwing.client.ftp.application.swing.ActionListeners.ConnectActionListener;
import com.lightwing.client.ftp.application.swing.view.ConnectView;

import javax.swing.*;

public class ConnectController
{
  private AppContainer appContainer;
  private JFrame connectFrame;

  public ConnectController(AppContainer appContainer)
  {
    this.appContainer = appContainer;
    initialiseConnectView();
  }

  public void destroySelf()
  {
    connectFrame.setVisible(false);
    connectFrame.dispose();
  }

  private void initialiseConnectView()
  {
    ConnectView connectView = new ConnectView();
    connectView.doLayout();

    connectFrame = new JFrame("FTP Connection");
    connectFrame.setSize(300,200);
    connectFrame.add(connectView);
    connectFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    connectFrame.setVisible(true);

    connectView.getConnectButton().addActionListener(new ConnectActionListener(connectView, appContainer));
  }
}
