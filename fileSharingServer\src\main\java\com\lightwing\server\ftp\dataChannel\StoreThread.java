package com.lightwing.server.ftp.dataChannel;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ServerSocket;

public class StoreThread implements Runnable
{
  private DataChannel dataChannel;
  private File newFile;

  public StoreThread(DataChannel dataChannel, File newFile)
  {
    this.dataChannel = dataChannel;
    this.newFile = newFile;
  }

  public void run()
  {
    ServerSocket dataSocket = dataChannel.getDataSocket();

    if (dataSocket != null)
    {
      try
      {
        FileOutputStream fileOutputStream = new FileOutputStream(newFile);
        InputStream inputStream = dataSocket.accept().getInputStream();

        byte[] inputFileByteArray = new byte[1024];
        int count;

        while ((count = inputStream.read(inputFileByteArray)) > 0)
        {
          dataChannel.incrementBytesTransferred(count);
          fileOutputStream.write(inputFileByteArray, 0, count);
        }


        inputStream.close();
        fileOutputStream.close();
        dataChannel.transferEnded(PreviousTransferStatus.SUCCESS);
      }
      catch (IOException e)
      {
        e.printStackTrace();
      }
    }
  }
}
