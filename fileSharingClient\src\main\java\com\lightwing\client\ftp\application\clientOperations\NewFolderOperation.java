package com.lightwing.client.ftp.application.clientOperations;

import com.lightwing.client.ftp.model.FtpSession;

import java.io.File;

public class NewFolderOperation
{
  private FtpSession ftpSession;

  public NewFolderOperation(FtpSession ftpSession)
  {
    this.ftpSession = ftpSession;
  }

  public void process(String newDirectoryName)
  {
    File newDir = new File(ftpSession.getClientWorkingDirectory() + "/" + newDirectoryName);

    if (!newDir.exists())
    {
      newDir.mkdirs();

      if (newDir.exists() && newDir.isDirectory())
      {
        ftpSession.setClientWorkingDirectory(newDir.getPath());
      }
    }
  }
}
