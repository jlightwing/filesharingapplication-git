package com.lightwing.server.ftp.commands;

import com.lightwing.server.ftp.models.FileWrapper;
import com.lightwing.server.ftp.session.FtpSession;
import com.lightwing.server.ftp.utils.Response;

import java.io.File;

public class DeleteFileCommand implements Command
{
  public static final String COMMAND = "DELE";

  public boolean isMe(String commandType)
  {
    return COMMAND.equals(commandType);
  }

  public boolean canAccess(boolean loggedIn)
  {
    return loggedIn;
  }

  public String process(String commandParam, FtpSession ftpSession)
  {
    File fileToDelete = new File(commandParam);
    FileWrapper workingDirectory = ftpSession.getFileWrapper();

    try
    {
      if (fileToDelete.exists() && fileToDelete.isFile() && fileToDelete.delete())
      {
        return Response.SUCCESS.setData(workingDirectory.getFile().getPath()).toString();
      }
      else
      {
        return Response.FILE_ACTION_FAULT.setData(commandParam).toString();
      }
    }
    catch (SecurityException e)
    {
      return Response.FILE_PERMISSION_ERROR.setData(commandParam).toString();
    }
  }
}
