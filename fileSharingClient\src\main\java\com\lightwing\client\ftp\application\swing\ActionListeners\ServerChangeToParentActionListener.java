package com.lightwing.client.ftp.application.swing.ActionListeners;

import com.lightwing.client.ftp.application.swing.ClientController;
import com.lightwing.client.ftp.model.FtpClient;
import com.lightwing.client.ftp.model.FtpSession;
import com.lightwing.client.ftp.model.Response;
import com.lightwing.client.ftp.application.swing.view.ClientView;

import java.awt.event.ActionEvent;
import java.io.IOException;

public class ServerChangeToParentActionListener extends AbstractActionListener
{
  public ServerChangeToParentActionListener(ClientController clientController, FtpSession ftpSession, FtpClient ftpClient, ClientView clientView)
  {
    super(clientController, ftpSession, ftpClient, clientView);
  }

  @Override
  public void actionPerformed(ActionEvent e)
  {
    try
    {
      Response response = new Response(ftpClient.changeToParentDirectory());
      updateView(response);
    }
    catch (IOException e2)
    {
      clientView.showMessageBox("Error sending request to server");
    }
  }
}