package com.lightwing.client.ftp.application.swing.view.progressBar;

public class StatusContainer
{
  private volatile long bytesTransferred;
  private long targetSize;

  public StatusContainer(long targetSize)
  {
    this.bytesTransferred = 0;
    this.targetSize = targetSize;
  }

  public synchronized int getProgress()
  {

    if (bytesTransferred == 0 || targetSize == 0)
    {
      return 0;
    }
    else
    {
      float ratio = (float) bytesTransferred / (float)targetSize;
      return Math.round(ratio * 100) % 101;
    }
  }

  public synchronized void setBytesTransferred(long bytesTransferred)
  {
    this.bytesTransferred = bytesTransferred;
  }

  public synchronized void setFinished()
  {
    setBytesTransferred(targetSize);
  }
}
