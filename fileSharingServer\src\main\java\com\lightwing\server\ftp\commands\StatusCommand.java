package com.lightwing.server.ftp.commands;

import com.lightwing.server.ftp.dataChannel.DataChannel;
import com.lightwing.server.ftp.dataChannel.DataChannelStatus;
import com.lightwing.server.ftp.dataChannel.PreviousTransferStatus;
import com.lightwing.server.ftp.session.FtpSession;
import com.lightwing.server.ftp.utils.Response;

public class StatusCommand implements Command
{
  public static final String COMMAND = "STATUS";

  public boolean isMe(String commandType)
  {
    return COMMAND.equals(commandType);
  }

  public boolean canAccess(boolean loggedIn)
  {
    return loggedIn;
  }

  public String process(String commandParam, FtpSession ftpSession)
  {
    DataChannel dataChannel = ftpSession.getDataChannel();
    DataChannelStatus dataChannelStatus = dataChannel.getStatus();
    PreviousTransferStatus previousTransferStatus = dataChannel.getPreviousTransferStatus();
    String bytesTransferred = String.valueOf(dataChannel.getBytesTransferred());

    if (dataChannelStatus.equals(DataChannelStatus.IN_PROGRESS))
    {
      return Response.STATUS_IN_PROGRESS.setData(bytesTransferred).toString();
    }
    else if (previousTransferStatus.equals(PreviousTransferStatus.SUCCESS))
    {
      return Response.STATUS_SUCCESS.setData(bytesTransferred).toString();
    }
    else if (previousTransferStatus.equals(PreviousTransferStatus.FAILURE))
    {
      return Response.STATUS_FAILURE.setData(bytesTransferred).toString();
    }
    else
    {
      return Response.STATUS_NO_STATUS.toString();
    }
  }
}