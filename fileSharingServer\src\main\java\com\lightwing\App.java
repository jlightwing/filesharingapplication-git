package com.lightwing;

import com.lightwing.server.ftp.FtpServer;
import com.lightwing.server.ftp.FtpServerImpl;

import java.io.FilePermission;
import java.io.IOException;
import java.security.SecurityPermission;

public class App
{
  public static void main(String[] args) throws IOException
  {
    FtpServer ftpServer = new FtpServerImpl(args);
    ftpServer.run();
  }
}
